#include "Piece.h"
#include <iostream>

int main()
{
	Piece piece(Piece::Body::Hollow, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
	std::cout << piece;
	return 0;
}