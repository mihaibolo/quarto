#include "Piece.h"
#include <cstdint>

Piece::Piece(Body body, Color color, Height height, Shape shape):
	m_body(body), m_color(color), m_height(height), m_shape(shape)
{
	sizeof(*this);
	static_assert(sizeof(*this) == 1, "This is not size 1.");
}

Piece::Body Piece::GetBody() const
{
	return Body();
}

Piece::Color Piece::GetColor() const
{
	return Color();
}

Piece::Height Piece::GetHeight() const
{
	return Height();
}

Piece::Shape Piece::GetShape() const
{
	return Shape();
}

std::ostream& operator<<(std::ostream& out, const Piece& piece)
{
	switch (piece.m_body)
	{
	case Piece::Body::Hollow:
		out << "Hollow ";
		break;
	case Piece::Body::Solid:
		out << "Solid ";
		break;
	}
	switch (piece.m_color)
	{
	case Piece::Color::Dark:
		out << "Dark ";
		break;
	case Piece::Color::Light:
		out << "Light ";
		break;
	}
	switch (piece.m_height)
	{
	case Piece::Height::Short:
		out << "Short ";
		break;
	case Piece::Height::Tall:
		out << "Tall ";
		break;
	}
	switch (piece.m_shape)
	{
	case Piece::Shape::Round:
		out << "Round ";
		break;
	case Piece::Shape::Square:
		out << "Square ";
		break;
	}
	// out << static_cast<int32_t>(piece.m_body);
	return out;
}
